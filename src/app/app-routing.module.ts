import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { AchievmentComponent } from './achievment/achievment.component';
import { SkillsComponent } from './skills/skills.component';
import { HomeComponent } from './home/home.component';
import { AcademicComponent } from './education/academic.component';
import { ContactComponent } from './contact/contact.component';


const routes: Routes = [
  {
    path: '',
    redirectTo:'home',
    pathMatch:'full'
  },
  {
    path: 'home',
    component:HomeComponent
  }, {
    path: 'academic',
    component:AcademicComponent
  }, {
    path: 'skills',
    component:SkillsComponent
  }, {
    path: 'achievement',
    component:AchievmentComponent
  },
  {
    path: 'details',
    component:DetailsComponent
  },  {
    path: 'contact',
    component:ContactComponent
  },
  {
    path: '**',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
