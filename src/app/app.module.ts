import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AchievmentComponent } from './achievment/achievment.component';
import { DetailsComponent } from './details/details.component';
import { SkillsComponent } from './skills/skills.component';
import { ContactComponent } from './contact/contact.component';
import { AcademicComponent } from './education/academic.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AchievmentComponent,
    DetailsComponent,
    SkillsComponent,
    ContactComponent,
    AcademicComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
