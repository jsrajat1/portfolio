# Portfolio

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).



To run this project on your system, first you need to download node.js on your system. This will automatically download npm on your system which helps to share and reuse code easily. Then download '@angular-cli' from npmjs.com. To do so, you need to run following command on your terminal "npm install @angular/cli --save". This should be downloaded on the same folder where the project is cloned. Likewise, you need to download rxjx from the npmjs.com. npmjs is a site where you find all your js tools and extension. For the notification animation you must dowload 'ngx-toastr' from npmjs.com and is mandatory. This is all for setting up your working environment on your systerm. After all the downloadings are done, serve this angular project from command line. By staying on the root directory, run 'ng serve' command to serve this project locally which is served at 'localhost:4200'. The default port number is 4200. And this is all done for running this project on your system.